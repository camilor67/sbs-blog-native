// /components/Post.js

import { getDate } from '../utils/utils';
import { Image, Text, StyleSheet, View, Button, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 10
  },
  text: {
    fontSize: 16,
    fontWeight: '300',
    paddingRight: 15,
    marginRight: 15
  },
  box: {
    flex: 1,
    height: 50,
    width: 50,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    overflow: 'hidden',
    marginVertical: 8,
    marginHorizontal: 16,
    padding: 18,
    borderWidth: 1,
    borderColor: "#e5e5e5",
    backgroundColor: "#fbfbfb",
    fontSize: 13
  },
  row: {
    flex: 1,
    paddingLeft: 15
  },
  button: {
    paddingTop: 8,
    marginTop: 15,
    width: 120,
    height: 35,
    backgroundColor: "#ff6900",
  },
  bText: {
    color: "#fff",
    textAlign: "center",
    fontWeight: "bold"
  }
});
export default function Post({ post, featuredMedia }) {
  return (
    <View style={styles.item}>
    
    <Image source={{uri: featuredMedia['media_details'].sizes.medium['source_url']}}
                style={{width: 180, height: 120}} />

    <View style={styles.row}>
      <Text style={styles.text}> {post.title} </Text>
      <TouchableOpacity activeOpacity={0.95} style={styles.button}>
          <Text style={styles.bText}>Read More</Text>
      </TouchableOpacity>
    </View>
    
    {/* <Link href={`/posts/${post.slug}`}>
      <a className="btn btn-primary">See more</a>
    </Link> */}
      {/* <div className="row g-0">
        <div className="col-md-4">
          <Link href={`/posts/${post.slug}`}>
            <a>
              <Image
                src={featuredMedia['media_details'].sizes.medium['source_url']}
                width={180}
                height={120}
                alt={featuredMedia['alt_text']}
              />
            </a>
          </Link>
        </div>
        <div className="col-md-8">
          <div className="card-body">
            <h5 className="card-title">{post.title.rendered}</h5>
            <div
              className="card-text"
              dangerouslySetInnerHTML={{ __html: post.excerpt.rendered }}
            ></div>
            <p className="card-text">
              <small className="text-muted">On {getDate(post.modified)}</small>
            </p>
            <Link href={`/posts/${post.slug}`}>
              <a className="btn btn-primary">See more</a>
            </Link>
          </div>
        </div>
      </div> */}
    </View>
  );
}