import axios from 'axios';
import * as React from 'react'
import { View, Text, StyleSheet, SafeAreaView, ScrollView, StatusBar, VirtualizedList, TouchableOpacity } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Header, HeaderProps, Icon } from 'react-native-elements';

import { getPosts } from '../utils/wordpress';

import Post from '../components/Post';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    marginHorizontal: 20,
  },
  headerRight: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 5,
  },
  heading: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold',
  },
  text: {
    fontSize: 40,
    fontWeight: '700',
  }
});

export default function Home({ posts, events }) {
  const BASE_URL = 'https://www.safestbettingsites.com/wp-json/wp/v2';
  const [jsxPosts, setJsxPosts] = React.useState([])
  const [postsData, setPostsData] = React.useState([])

  const getItem = (data, index) => ({
    id: data[index].id,
    key: data[index].id,
    title: data[index].title.rendered,
    featuredMedia: data[index]['_embedded']['wp:featuredmedia'][0]
  });

  const getItemCount = (data) => data.length;

  React.useEffect(() => {BASE_URL + '/posts?_embed'
    axios.get(BASE_URL + '/posts?_embed')
    .then(response => {
      const posts = response.data;
      setPostsData(posts)
      const jsxPostsa = posts.map((post) => {
        const featuredMedia = post['_embedded']['wp:featuredmedia'][0];
        return <Post post={post} featuredMedia={featuredMedia} key={post.id} />;
      });
      setJsxPosts(jsxPostsa)
    });
  }, []);

  return (
    <SafeAreaProvider style={styles.container}>
      <Header
        leftComponent={{
          icon: 'menu',
          color: '#fff',
        }}
        rightComponent={
            <View style={styles.headerRight}>
              <TouchableOpacity>
                <Icon name="description" color="white" />
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginLeft: 10 }}
              >
              <Icon type="antdesign" name="rocket1" color="white" />
              </TouchableOpacity>
            </View>
        }
        centerComponent={{ text: 'Safestbettingsites', style: styles.heading }}
      />
      <Text style={styles.text}> SBS Blog posts </Text>
      <VirtualizedList
        data={postsData}
        initialNumToRender={4}
        renderItem={({ item }) => <Post post={item} featuredMedia={item.featuredMedia} key={item.id} />}
        keyExtractor={item => item.key}
        getItemCount={getItemCount}
        getItem={getItem}
      />
    </SafeAreaProvider>
  );
}
